/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.*;
import java.util.*;
import model.Customer;

/**
 *
 * @author tatar
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (name,tel) VALUES (?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM customer";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                Customer customer = new Customer(id, name, tel);
                list.add(customer);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        ArrayList list = new ArrayList();
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM customer WHERE id = " + id;
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                int cid = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                Customer customer = new Customer(cid, name, tel);
                list.add(customer);
                return customer;
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM customer WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row : " + row);
            System.out.println("-----------------------");

            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "Yui Hirakawa", "0547789853"));
        System.out.println("id: " + id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last product : " + lastCustomer);
        lastCustomer.setTel("0851111111");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer : " + updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer : " + deleteCustomer);
    }
}
