/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_11.poc;

import java.util.*;
import java.sql.*;
import database.Database;

/**
 *
 * @author tatar
 */
public class UpdateProduct {

    public static void main(String[] args) {
        SelectProduct.select();
        Scanner kb = new Scanner(System.in);
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            System.out.println("Select product ID \nID : ");
            int selid = kb.nextInt();
            System.out.println("-----------------------");
            System.out.println("Update \nProduct name : ");
            String name = kb.next();
            System.out.println("Price : ");
            double price = kb.nextDouble();

            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setDouble(2, price);
            stmt.setInt(3, selid);
            int row = stmt.executeUpdate();
            System.out.println("Affect row : " + row);
            System.out.println("-----------------------");

            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        System.out.println("Update complete :)");
        /*
        System.out.println("You data in product table."
                + "\n-----------------------");
        SelectProduct.select();*/

    }
}
