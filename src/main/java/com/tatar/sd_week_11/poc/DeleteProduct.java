/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_11.poc;

import java.util.*;
import java.sql.*;
import database.Database;

/**
 *
 * @author tatar
 */
public class DeleteProduct {

    public static void main(String[] args) {
        SelectProduct.select();
        Scanner kb = new Scanner(System.in);
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            System.out.println("Select product ID to Delete\nID : ");
            int selid = kb.nextInt();
            System.out.println("-----------------------");
            String sql = "DELETE FROM product WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, selid);
            int row = stmt.executeUpdate();
            System.out.println("Affect row : " + row);
            System.out.println("-----------------------");

            stmt.close();

        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        System.out.println("Delete complete :)");
        /*System.out.println("You data in product table."
                + "\n-----------------------");
        SelectProduct.select(); */

    }
}
