/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_11.poc;

import database.Database;
import java.sql.*;
import java.util.*;
import model.Product;

/**
 *
 * @author tatar
 */
public class InsertProduct {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            System.out.println("Insert \nProduct name : ");
            String name = kb.next();
            System.out.println("Price : ");
            double price = kb.nextDouble();

            String sql = "INSERT INTO product (name,price) VALUES (?,?)";
            stmt = conn.prepareStatement(sql);
            Product product = new Product(-1, name, price);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            int id = 1;
            if (rs.next()) {
                id = rs.getInt(1);
            }
            System.out.println("Affect row : " + row + " id : " + id);
            System.out.println("-----------------------");

            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        System.out.println("Insert complete :)\nYou data in product table.");
        System.out.println("-----------------------");
        SelectProduct.select();

    }
}
