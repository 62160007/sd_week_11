/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_11.poc;

import database.Database;
import java.sql.*;
import java.util.*;
import model.Product;

/**
 *
 * @author tatar
 */
public class SelectProduct {

    public static void main(String[] args) {
        select();
    }

    public static void select() {
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM product";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                //System.out.println("ID : "+id);
                String name = rs.getString("name");
                //System.out.println("Name : "+name);
                Double price = rs.getDouble("price");
                Product product = new Product(id, name, price);
                System.out.println(product);
                //System.out.println("Price : "+price+" Baht");                    
                System.out.println("-----------------------");
            }
            //System.out.println(rs);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Select complete :)");

        db.close();
    }
}
